import datetime
import socket
import urllib.error
from unittest.mock import patch

import bs4
import pyPodcastParser.Podcast
from pytest import raises

from getpodcast import Options, SkipPodcast, process_podcast_item


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.utime")
def test_process_podcast_item_no_enclosure_url(utime, validateFile, try_download_item):
    pod = ""
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    headers = {}
    fromdate = None
    options = {f: None for f in Options._fields}
    process_podcast_item(pod, item, headers, fromdate, options)

    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_not_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
def test_process_podcast_item_validate_existing(
    utime, isfile, getsize, validateFile, try_download_item
):
    isfile.side_effect = lambda fn: fn == "somefile.ogg"
    getsize.side_effect = lambda fn: 102 if fn == "somefile.ogg" else 0
    pod = "test"
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "uri://test1.ogg"
    item.title = "test 1"
    item.date_time = datetime.datetime(2022, 1, 1)
    headers = {}
    fromdate = None
    options = {f: None for f in Options._fields}
    options.update(template="somefile.ogg", root_dir=".")
    args = Options(**options)
    process_podcast_item(pod, item, headers, fromdate, args)

    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_called_once()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
def test_process_podcast_item_onlynew_skip_podcast(
    utime, isfile, getsize, validateFile, try_download_item
):
    isfile.side_effect = lambda fn: fn == "somefile.ogg"
    getsize.side_effect = lambda fn: 102 if fn == "somefile.ogg" else 0
    pod = "test"
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "uri://test1.ogg"
    item.title = "test 1"
    item.date_time = datetime.datetime(2022, 1, 1)
    headers = {}
    fromdate = None
    options = {f: None for f in Options._fields}
    options.update(template="somefile.ogg", root_dir=".", onlynew=True)
    args = Options(**options)

    with raises(SkipPodcast):
        process_podcast_item(pod, item, headers, fromdate, args)

    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_called_once()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
def test_process_podcast_item_resume_download(
    utime, isfile, getsize, validateFile, try_download_item
):
    isfile.side_effect = lambda fn: fn == "somefile.ogg"
    getsize.side_effect = lambda fn: 102 if fn == "somefile.ogg" else 0
    validateFile.return_value = False
    try_download_item.return_value = True, 0  # cancel validate and exit

    pod = "test"
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "uri://test1.ogg"
    item.title = "test 1"
    item.date_time = datetime.datetime(2022, 1, 1)
    headers = {}
    fromdate = None
    options = {f: None for f in Options._fields}
    options.update(template="somefile.ogg", root_dir=".")
    args = Options(**options)

    process_podcast_item(pod, item, headers, fromdate, args)

    utime.assert_not_called()
    try_download_item.assert_called_once()
    validateFile.assert_called_once()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
def test_process_podcast_item_download(
    utime, isfile, getsize, validateFile, try_download_item
):
    isfile.return_value = False
    getsize.side_effect = lambda fn: 113 if fn == "somefile.ogg" else 0
    validateFile.return_value = True
    try_download_item.return_value = False, 0  # download success

    pod = "test"
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "uri://test1.ogg"
    item.title = "test 1"
    item.date_time = datetime.datetime(2022, 1, 1)
    headers = {}
    fromdate = None
    options = {f: None for f in Options._fields}
    options.update(template="somefile.ogg", root_dir=".")
    args = Options(**options)

    process_podcast_item(pod, item, headers, fromdate, args)

    utime.assert_called_once()
    try_download_item.assert_called_once()
    validateFile.assert_called_once()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
def test_process_podcast_item_validate_errors(
    utime, isfile, getsize, validateFile, try_download_item
):
    isfile.side_effect = lambda fn: fn == "somefile.ogg"
    getsize.side_effect = lambda fn: 102 if fn == "somefile.ogg" else 0
    validateFile.side_effect = [
        urllib.error.URLError("unittest"),
        urllib.error.HTTPError(None, None, None, None, None),
        socket.timeout(),
    ]
    pod = "test"
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "uri://test1.ogg"
    item.title = "test 1"
    item.date_time = datetime.datetime(2022, 1, 1)
    headers = {}
    fromdate = None
    options = {f: None for f in Options._fields}
    options.update(template="somefile.ogg", root_dir=".")
    args = Options(**options)

    for errs in range(3):
        process_podcast_item(pod, item, headers, fromdate, args)

    utime.assert_not_called()
    try_download_item.assert_not_called()
    validateFile.assert_called()


@patch("getpodcast.try_download_item")
@patch("getpodcast.validateFile")
@patch("os.path.getsize")
@patch("os.path.isfile")
@patch("os.utime")
def test_process_podcast_item_download_errors(
    utime, isfile, getsize, validateFile, try_download_item
):
    isfile.return_value = False
    getsize.side_effect = lambda fn: 113 if fn == "somefile.ogg" else 0
    try_download_item.return_value = False, 0  # download success
    validateFile.side_effect = [
        # urllib.error.URLError("unittest"),
        urllib.error.HTTPError(None, None, None, None, None),
        socket.timeout(),
    ]
    pod = "test"
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "uri://test1.ogg"
    item.title = "test 1"
    item.date_time = datetime.datetime(2022, 1, 1)
    headers = {}
    fromdate = None
    options = {f: None for f in Options._fields}
    options.update(template="somefile.ogg", root_dir=".")
    args = Options(**options)
    for errs in range(2):
        process_podcast_item(pod, item, headers, fromdate, args)

    utime.assert_not_called()
    try_download_item.assert_called()
    validateFile.assert_called()
