import socket
import urllib.error
from unittest.mock import mock_open, patch
from urllib.response import addinfo

import bs4
import pyPodcastParser.Podcast

from getpodcast import downloadFile, resumeDownloadFile, try_download_item


def _make_urlopen_response(headers):
    fp = mock_open()()
    fp.closed = False
    return addinfo(fp, headers)


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("builtins.open", new_callable=mock_open)
@patch("os.makedirs")
def test_downloadFile_get_data(makedirs, _open, urlopen, Request):
    urlopen.return_value = _make_urlopen_response({})

    headers = {}
    newfolder = "./podcasts/P1/2020"
    newfilename = newfolder + "/EP1.ogg"
    enclosure_url = "URN://P1"

    downloadFile(newfilename, enclosure_url, headers)

    urlopen.assert_called_once()
    makedirs.assert_called_once_with(newfolder)
    _open.assert_called_once_with(newfilename, "wb")


@patch("urllib.request.Request")
@patch("urllib.request.urlopen")
@patch("builtins.open", new_callable=mock_open)
def test_resumeDownloadFile_get_data(_open, urlopen, Request):
    urlopen.return_value = _make_urlopen_response({})

    headers = {}
    newfolder = "./podcasts/P1/2020"
    newfilename = newfolder + "/EP1.ogg"
    enclosure_url = "URN://P1"

    resumeDownloadFile(newfilename, enclosure_url, headers)

    urlopen.assert_called()
    _open.assert_called_once_with(newfilename, "ab+")


# TODO:
# test_resumeDownloadFile_resume_not_possible
# test_resumeDownloadFile_resume_not_possible
# test_resumeDownloadFile_resume_add_bytes


@patch("getpodcast.resumeDownloadFile")
@patch("getpodcast.downloadFile")
def test_try_download_item_start_resume_from_10(_download, _resume):
    headers = {}
    newfilename = "./podcasts/P1/2020/EP1.ogg"
    filelength = 10
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())

    cancel_validate, newfilelength = try_download_item(
        filelength, newfilename, item, headers
    )

    assert not cancel_validate
    assert newfilelength == filelength
    _download.assert_not_called()
    _resume.assert_called_once_with(newfilename, None, headers)


@patch("getpodcast.resumeDownloadFile")
@patch("getpodcast.downloadFile")
def test_try_download_item_start_download(_download, _resume):
    headers = {}
    newfilename = "./podcasts/P1/2020/EP1.ogg"
    filelength = 0
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())

    cancel_validate, newfilelength = try_download_item(
        filelength, newfilename, item, headers
    )

    assert not cancel_validate
    assert newfilelength == filelength
    _resume.assert_not_called()
    _download.assert_called_once_with(newfilename, None, headers)


@patch("urllib.request.Request")
@patch("os.makedirs")
def test_try_download_item_url_error(makedirs, request):
    def errors():
        yield urllib.error.URLError("unittest")
        yield urllib.error.HTTPError(None, None, None, None, None)
        yield socket.timeout()

    request.side_effect = errors()
    headers = {}
    newfilename = "./podcasts/P1/2020/EP1.ogg"
    filelength = 0
    item = pyPodcastParser.Podcast.Item(bs4.BeautifulSoup())
    item.enclosure_url = "URN://EP1"

    for errs in range(3):
        cancel_validate, newfilelength = try_download_item(
            filelength, newfilename, item, headers
        )

        assert cancel_validate
        assert newfilelength == filelength
